const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port = process.env.PORT || 3000;
var app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');

app.use((req,res,next) => {
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}`

    console.log(log);
    fs.appendFile('server.log', log + '\n', (err) => {
      if (err) {
        console.log('Unable to append to server.log.');
      }
    });
    next();
});

// app.use((req,res,next) => {
//   setTimeout(() => {
//     res.render('maintenace.hbs', {
//       pageTitle: 'Undergoing Maintenace'
//     });
//   },1500);
// });

app.use(express.static(__dirname + '/public_html'));

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase();
});



app.listen(port, () => {
  console.log(`Server up and running on port ${port}...`);
});
